const fsPromises = require("fs").promises
const path = require("path")

function createRandomjsonFiles(){
    fsPromises.mkdir("./randomFiles",{recursive:true})
    .then(()=>{
        console.log("directory created")
    })
    .catch((error)=>{
        console.log(error)
    })
    for(let index = 1;index < 10;index++){
        let content = `This is file ${index}`;
        let fileName = `file${index}.txt`;
        let dir = "./randomFiles";
        let filePath=path.join(dir,fileName);
        let file=`file${index}`

        fsPromises.writeFile(filePath,JSON.stringify(content))
        .then(()=>{
            console.log(`${file} created successfully`)
        }).then(()=>{
            fsPromises.unlink(filePath)
             .then(()=>{
                console.log(`${file} Deleted successfully`)
            }).catch((error)=>{
                console.log(error)
            })
        }).catch((error)=>{
            console.log(error)
        })
            
        }
        fsPromises.rm("./randomFiles",{recursive:true})
        .then(()=>{
            console.log("Directory deleted SuccessFully")
        }).catch((error)=>{
            console.log(error)

        })
}
module.exports = createRandomjsonFiles;



